Gem::Specification.new do |s|
  s.name        = 'object_cacheable'
  s.version     = '1.1.2'
  s.summary     = "Object-level caching"
  s.description = "A gem for object-level caching."
  s.authors     = ['Linh Chau']
  s.email       = 'chauhonglinh@gmail.com'
  s.files       = [
                    './Gemfile', './object_cacheable.gemspec', 'lib/object_cacheable.rb',
                    'conf/cache_configs.yml', 'lib/cache_configuration.rb', 'lib/cache_utils.rb', 'lib/cacheable.rb',
                    'test/models/cacheable_test.rb'
                  ]
  s.homepage    = 'https://github.com/linhchauatl/object_cacheable.git'
  s.license     = 'MIT'
end

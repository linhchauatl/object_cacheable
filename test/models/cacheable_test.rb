# Run test from the command line, gem root
# bundle exec ruby -Ilib test/models/cacheable_test.rb 

require 'minitest/autorun'
require 'minitest-spec-context'
require 'cacheable'
require 'mocha'

class TestCacheable
  include Cacheable
  attr_accessor :id, :value_1, :value_2, :value_3

  # This method mimics a real call to database or webservices
  def self.fetch(attributes, args)
    # Assume that the combination of (value_1 + value_2) will uniquely identify one instance - for testing purpose
    # For testing convenience, this implementation returns the same value object for the following calls:
    # obj1 = TestCacheable.fetch_by_id(1)
    # obj2 = TestCacheable.fetch_by_value_1_and_value_2('value 1', 'value 2')

    # The calls with different ids will return different objects

    if ( (args.size == 1) or (args.size == 3) )
      return TestCacheable.new(*args)
    elsif (args.size == 2)
      # Because the initilize always expects value 1 before value 2, so if we have attributes sent as ['value_2', 'value_1'], we have to reverse the args
      created_args = (attributes == ['value_1', 'value_2'])? args : args.reverse
      
      return (args.sort == ['value 1', 'value 2'])? TestCacheable.new(1, *created_args) : TestCacheable.new( (args[0].to_s.hash + args[1].to_s.hash), *created_args)
    end

    return TestCacheable.new(100, 'test value 1', 'test value 2')

  end

  def initialize(an_id, a_value_1 = 'value 1', a_value_2 = 'value 2')
    self.id = an_id

    self.value_1 = a_value_1
    self.value_2 = a_value_2 


    self.value_3 = 'Original Value 3'
  end

  def ==(obj)
    self.id == obj.id && self.value_1 == obj.value_1 && self.value_2 == obj.value_2 && self.value_3 == obj.value_3
  end

end


describe Cacheable do

  context 'fetch object' do
    it 'calls the real fetch when the object is not in cache, then puts object in the cache' do

      # First, make sure that there is nothing in cache
      cached_obj = Cacheable::cache.fetch('TestCacheable_id_1')
      cached_obj.must_be_nil

      # Call the method that fetches the object and puts in cache
      obj = TestCacheable.fetch_by_id(1)
      obj.wont_be_nil

      cached_obj = Cacheable::cache.fetch('TestCacheable_id_1')
      cached_obj.must_equal obj

      # Make sure that it doesn't make a real call to fetch the second time
      TestCacheable.stub(:fetch, nil) do 
        result_obj = TestCacheable.fetch_by_id(1)
        result_obj.must_equal obj
      end

      # clean the cache after finish
      obj.delete_from_cache
    end

    it 'fetches and caches different objects separately' do
      obj3 = TestCacheable.fetch_by_id(3)
      obj3.wont_be_nil

      obj4 = TestCacheable.fetch_by_id(4)
      obj4.wont_be_nil

      # Fetched objects must be different
      obj3.wont_equal obj4

      #Cached objects must be different
      cached_obj3 = Cacheable::cache.fetch('TestCacheable_id_3')
      cached_obj3.must_equal obj3

      cached_obj4 = Cacheable::cache.fetch('TestCacheable_id_4')
      cached_obj4.must_equal obj4

      cached_obj3.wont_equal cached_obj4

      # clean the cache after finish
      obj3.delete_from_cache
      obj4.delete_from_cache
    end

    it 'fetches the same object with different ordered combinations of keys' do
      obj1 = TestCacheable.fetch_by_value_1_and_value_2('key 1', 'key 2')
      obj2 = TestCacheable.fetch_by_value_2_and_value_1('key 2', 'key 1')

      obj1.must_equal obj2

      cached_obj1 = Cacheable::cache.fetch('TestCacheable_value_1_value_2_key 1_key 2')
      cached_obj2 = Cacheable::cache.fetch('TestCacheable_value_2_value_1_key 2_key 1')

      cached_obj1.must_equal cached_obj2

      # clean the cache after finish - make sure that obj1 and obj2 are both deleted from cache
      obj1.delete_from_cache
      cached_obj2 = Cacheable::cache.fetch('TestCacheable_value_2_value_1_key 2_key 1')
      cached_obj2.must_be_nil

    end

    it 'fetches two different objects with revert values for keys' do
      # These 2 different objects have revert values for keys, 
      # for example [firstname, lastname] = [John, Smith] and [fistname, lastname] = [Smith, John]
      # Unlikely to happen, but we will test anyway
      obj1 = TestCacheable.fetch_by_value_1_and_value_2('value 1234', 'value 5678')
      obj2 = TestCacheable.fetch_by_value_1_and_value_2('value 5678', 'value 1234')

      # fetched objects are different
      obj1.wont_equal obj2

      cached_obj1 = Cacheable::cache.fetch('TestCacheable_value_1_value_2_value 1234_value 5678')
      cached_obj1.must_equal obj1

      cached_obj2 = Cacheable::cache.fetch('TestCacheable_value_1_value_2_value 5678_value 1234')
      cached_obj2.must_equal obj2

      # cached objects are different
      cached_obj1.wont_equal cached_obj2

       # clean the cache after finish
      obj1.delete_from_cache
      obj2.delete_from_cache
    end

  end

  context 'update object' do
    it 'updates all instances of object in the cache' do
      obj1 = TestCacheable.fetch_by_id(1)
      obj2 = TestCacheable.fetch_by_value_1_and_value_2('value 1', 'value 2')

      # We expect that obj1 and obj2 are 2 cached instances of the same object:
      obj1.must_equal obj2

      # Now update obj1 and expect the value is changed in the cache
      obj1.value_3 = 'New value 3'
      obj1.update_cache

      cached_obj1 = Cacheable::cache.fetch('TestCacheable_id_1')
      cached_obj1.value_3.must_equal 'New value 3'

      # We also expect that the obj_2 value_3 is also changed
      cached_obj2 = Cacheable::cache.fetch('TestCacheable_value_1_value_2_value 1_value 2')
      cached_obj2.value_3.must_equal 'New value 3'

       # clean the cache after finish
      obj1.delete_from_cache
      
    end
  end

  context 'delete object' do
    it 'deletes all instances of object in the cache' do
      obj1 = TestCacheable.fetch_by_id(1)
      obj2 = TestCacheable.fetch_by_value_1_and_value_2('value 1', 'value 2')

      # We expect that obj1 and obj2 are 2 cached instances of the same object:
      obj1.must_equal obj2

      # Delete obj1
      obj1.delete_from_cache

      cached_obj1 = Cacheable::cache.fetch('TestCacheable_id_1')
      cached_obj1.must_be_nil

      # The obj2 in the cache now must be nil too
      cached_obj2 = Cacheable::cache.fetch('TestCacheable_value_1_value_2_value 1_value 2')
      cached_obj2.must_be_nil

    end
  end

end

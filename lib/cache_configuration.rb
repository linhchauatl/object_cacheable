require 'yaml'
require "#{File.dirname(__FILE__)}/cache_utils"

module Cacheable
  def self.cache
    CacheConfiguration.cache
  end
  
  def self.logger
    CacheConfiguration.logger
  end
  
  class CacheConfiguration
    @@cache_instance = nil
    @@logger_instance = nil
    
    class << self
      def load_config
        base_path = File.expand_path('.')
        config_file = nil
        ['conf', 'config'].each do |sub_path|
          if File.exist?("#{base_path}/#{sub_path}/cache_configs.yml")
            config_file = "#{base_path}/#{sub_path}/cache_configs.yml"
          end
        end

        raise("You must have the file cache_configs.yml in either 'conf' or 'config' directory of your application") unless config_file

        configs = YAML.load_file(config_file)
        env = environment
        
        @@cache_instance  = eval(configs[env]['cache'])
        @@logger_instance = eval(configs[env]['logger'])
      end
      
      def environment
        return Rails.env if defined? Rails
        return ENV['RACK_ENV'] if ENV['RACK_ENV']
        'test'
      rescue => error
        'test'
      end
      
      def cache
        load_config if @@cache_instance.nil?
        @@cache_instance
      end
      
      def logger
        load_config if @@logger_instance.nil?
        @@logger_instance
      end
    end # class methods
  end # class CacheConfiguration
end
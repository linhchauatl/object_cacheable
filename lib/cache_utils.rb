module Cacheable
  class Utils
    def self.test_cache
      Cacheable::TestCache.instance
    end
    
    def self.test_logger
      Cacheable::TestLogger.instance  
    end
  end
  
  class TestCache
    @@cache_storage = {}
    @@cache_instance = nil
    
    def self.instance
      @@cache_instance ||= TestCache.new
    end
    
    def fetch(key)
      @@cache_storage[key]
    end
    
    def write(key, value, options = {})
      @@cache_storage[key] = value
    end
    
    def delete(key)
      @@cache_storage.delete(key)
    end
    
    def keys
      @@cache_storage.keys
    end
  end
  
  class TestLogger
    @@logger_instance = nil
    
    def self.instance
      @@logger_instance ||= TestLogger.new
    end
    
    def info(message)
      puts message
    end
    
    alias_method :warning, :info
    alias_method :error, :info
    
  end
end
[![Gem Version](https://badge.fury.io/rb/object_cacheable.svg)](http://badge.fury.io/rb/object_cacheable)

This is a gem that provides Ruby objects with the capability of object caching. This gem is framework-agnostic, so it can be used in all the types of Ruby applications.

### Installation

Use **gem**:
```
gem install object_cacheable
```

Use **bundle**:<br/>
In Gemfile, write:
```
gem 'object_cacheable'
```

In your application, write:
```
require 'object_cacheable'
```

In your application, there must be a directory called **conf** or **config** (either will work) that contains a file called [**cache_configs.yml**](https://github.com/linhchauatl/object_cacheable/blob/master/conf/cache_configs.yml).<br/>

Object that wants to be cached must include Cacheable, 

    class Person
        include Cacheable
    end


and implement the method:

    def self.fetch(attributes,args)
        # Your code to do the real object retrieval here
        # It can be database call, webservices call or complex object builder
        # The first argument attributes is an array of attribute names that we want to retrieve the object by
        # The second argument args is an array of values of the attributes
        # for example Person.fetch(['first_name', 'last_name'], ['Linh', 'Chau'])
        # But the combinations of attributes must be able to identify a unique object
    end


When a Class includes Cacheable and defines the method "fetch", it's instances will become cacheable, and all the cache operations will be handled by the module Cacheable.

Testing
`bundle exec ruby -Ilib test/models/cacheable_test.rb`